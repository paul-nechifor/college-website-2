# Second College Website

My second college website which I hosted on the students' server. This is from
2009 and it's also written in PHP. Also see [my first college
website][first-version].

![Screenshot](screenshot.png)

## Run it

Run with:

    php -S localhost:8000

and navigate to [localhost:8000](http://localhost:8000).

## License

MIT

[first-version]: https://github.com/paul-nechifor/college-website
